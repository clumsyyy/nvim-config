--[[
require("telescope").setup {
	extensions = {
		file_browser = {
			theme = "ivy",
			hijack_netrw = true,
			mappings = {
				-- ["i"] = {}, -- sample 
			}
		}
	}
}

require("telescope").load_extension "file_browser"
]]--

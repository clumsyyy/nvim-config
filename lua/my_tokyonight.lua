require("tokyonight").setup({
	style = "night",
	transparent = false,
	terminal_colors = true,
	styles = {
		comments = { italic = true },
		keywords = { italic = true },
		functions = {},
		variables = {},
		sidebars = "dark",
		floats = "dark",
	},
	dim_inactive = true,
	lualine_bold = true,
})

-- vim.cmd [[colorscheme tokyonight-night]]

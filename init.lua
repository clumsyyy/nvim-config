-- Disable mouse for the non-sadists
vim.opt.mouse = nil

require("my_keymaps")
require("my_generic")
require("my_lualine")
require("my_tokyonight")
require("my_telescope")


vim.cmd [[packadd packer.nvim]]
return require("packer").startup(function(use)
	-- Packer manages itself
	use "wbthomason/packer.nvim"
	
	-- telescope
	use {
		"nvim-telescope/telescope.nvim",
		requires = {{ "nvim-lua/plenary.nvim" }}
	}

	-- telescope file browser
	use {
		"nvim-telescope/telescope-file-browser.nvim",
		requires = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
	}

	-- lualine
	use {
		"nvim-lualine/lualine.nvim",
		requires = { "nvim-tree/nvim-web-devicons", opt = true }
	}

    -- treesitter
    use ("nvim-treesitter/nvim-treesitter", {run = ":TSUpdate"})
    
    use {
        "nvim-tree/nvim-tree.lua", requires = { "nvim-tree/nvim-web-devicons" }
    }

	-- tokyonight theme
	use "folke/tokyonight.nvim"

	-- bookmark files
	use "theprimeagen/harpoon"

    use "theprimeagen/vim-be-good"

	-- lsp zero
	use {
		"VonHeikemen/lsp-zero.nvim",
		requires = {
			-- lsp support
			{ "neovim/nvim-lspconfig" },
			{ "williamboman/mason.nvim" },
			{ "williamboman/mason-lspconfig.nvim" },
			
			-- autocompletion
			{ "hrsh7th/nvim-cmp" },
			{ "hrsh7th/cmp-buffer" },
			{ "hrsh7th/cmp-path" },
			{ "saadparwaiz1/cmp_luasnip" },
			{ "hrsh7th/cmp-nvim-lsp" },
			{ "hrsh7th/cmp-nvim-lua" },

			-- snippet support
			{ "L3MON4D3/LuaSnip" },
			{ "rafamadriz/friendly-snippets" },

		}
	}
end)


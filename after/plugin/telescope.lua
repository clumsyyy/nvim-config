local builtin = require("telescope.builtin")

-- Disabled to force pf keybinding below
-- vim.keymap.set("n", "<leader>t", ":Telescope find_files<CR>")

vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
vim.keymap.set("n", "<<leader>fg", builtin.git_files, {})
vim.keymap.set("n", "<leader>fs", function() 
	builtin.grep_string({ search = vim.fn.input("grep > ") })
end)

require("telescope").setup {
	extensions = {
		file_browser = {
			theme = "ivy",
			hijack_netrw = true,
			mappings = {
				-- ["i"] = {}, -- sample 
			}
		}
	}

}

require("telescope").load_extension "file_browser"

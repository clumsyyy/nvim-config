
vim.g.mapleader = "\\" -- backslash

vim.keymap.set("n", "<leader>t", vim.cmd.Ex)
vim.keymap.set("n", "<leader>s", ":w<CR>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

vim.keymap.set("n", "gg=GG", "<leader>it<CR>")

-- Disable arrow keys
vim.cmd[[nnoremap <Up> <Nop>]]
vim.cmd[[nnoremap <Down> <Nop>]]
vim.cmd[[nnoremap <Left> <Nop>]]
vim.cmd[[nnoremap <Right> <Nop>]]

vim.cmd[[inoremap <Up> <Nop>]]
vim.cmd[[inoremap <Down> <Nop>]]
vim.cmd[[inoremap <Left> <Nop>]]
vim.cmd[[inoremap <Right> <Nop>]]

vim.cmd[[cnoremap <Up> <Nop>]]
vim.cmd[[cnoremap <Down> <Nop>]]
vim.cmd[[cnoremap <Left> <Nop>]]
vim.cmd[[cnoremap <Right> <Nop>]]

vim.cmd[[vnoremap <Up> <Nop>]]
vim.cmd[[vnoremap <Down> <Nop>]]
vim.cmd[[vnoremap <Left> <Nop>]]
vim.cmd[[vnoremap <Right> <Nop>]]
